# radar_interface

Forked from https://bitbucket.org/unizg-fer-lamor/radar_interface.git

Adds a new node to parse CAN frames without the CAN device to be directly connected.

Example:

`roslaunch radar_interface pe_can_esr_parser.launch`

The example above will expect can frames in the topic `can_tx` under the namespace `esr_front`

It will also launch the originally developed visualizer by unizg.