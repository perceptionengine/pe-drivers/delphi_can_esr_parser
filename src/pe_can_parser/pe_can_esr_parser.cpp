/*
 * Copyright 2020 Perception Engine. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * v1.0 amc-nu (abrahammonrroy@yahoo.com)
 */


#include <ros/ros.h>
#include <can_msgs/Frame.h>
#include "radar_interface/delphi_esr/esr_tracks_can.h"
#include "radar_interface/can_tools.h"

namespace radar_interface
{
  class PeCanEsrParser
  {
  public:
    PeCanEsrParser();
    void spin();
  private:

    ros::NodeHandle nh_, private_nh_;

    radar_interface::RadarTrackArray tracks_msg_;

    ros::Publisher track_array_topic_;
    ros::Subscriber can_frame_sub_;

    bool first_track_arrived_;
    bool last_track_arrived_;
    int track_count_;
    int track_extras_count_;
    std::string radar_name_;

    void frameCallback(const can_msgs::FrameConstPtr &f);

    void parseTrack(const can::Frame &f);

    void parseTrackExtras(const can::Frame &f);

    void aggregateTracks(const can::Frame &f);
  };


  PeCanEsrParser::PeCanEsrParser() :
      nh_(), private_nh_("~")
  {
    std::string can_frame_topic = "can_tx";
    track_array_topic_ = nh_.advertise<radar_interface::RadarTrackArray>("radar_tracks", 10);
    ROS_INFO("[%s]Listening on %s for CAN frames", ros::this_node::getName().c_str(), can_frame_topic.c_str());
    can_frame_sub_ = nh_.subscribe(can_frame_topic, 1, &PeCanEsrParser::frameCallback, this);
    tracks_msg_.tracks.resize(ESR_MAX_TRACK_NUMBER);

    tracks_msg_.header.frame_id = "radar";
  }

  void PeCanEsrParser::spin()
  {
    ROS_INFO("[%s] Initialized. Waiting for CAN frames...", ros::this_node::getName().c_str());
    ros::spin();
  }

  void PeCanEsrParser::frameCallback(const can_msgs::FrameConstPtr &f)
  {
    can::Frame frame;
    frame.dlc = f->dlc;
    frame.id = f->id;
    frame.data = f->data;
    frame.is_error = f->is_error;
    frame.is_extended = f->is_extended;
    frame.is_rtr = f->is_rtr;
    if (!frame.isValid())
    {
      ROS_ERROR("Invalid frame from SocketCAN: id: %#04x, length: %d, "
                    "is_extended: %d, is_error: %d, is_rtr: %d",
                f->id, f->dlc, f->is_extended, f->is_error, f->is_rtr);
      return;
    } else
    {
      if (f->is_error)
      {
        ROS_WARN(
            "Received frame is error"); //: %s", can::tostring(f, true).c_str());
      }
    }

    if (f->id >= ESR_TRACK_START && f->id <= ESR_TRACK_END)
    {
      parseTrack(frame);
      aggregateTracks(frame);
    }
    if (f->id == ESR_TRACK_EXTRAS)
    {
      parseTrackExtras(frame);
      aggregateTracks(frame);
    }
  }

  void PeCanEsrParser::parseTrack(const can::Frame &f)
  {
    int track_id, status;
    float range, range_rate, range_accel, azimuth, lat_rate, width;
    track_id = f.id - ESR_TRACK_START;
    ESR_TRACK_RANGE.parseValue(f, &range);
    ESR_TRACK_RANGE_RATE.parseValue(f, &range_rate);
    ESR_TRACK_RANGE_ACCEL.parseValue(f, &range_accel);
    ESR_TRACK_ANGLE.parseValue(f, &azimuth);
    // std::cout << azimuth << std::endl;

    ESR_TRACK_LAT_RATE.parseValue(f, &lat_rate);
    ESR_TRACK_STATUS.parseValue(f, &status);
    ESR_TRACK_WIDTH.parseValue(f, &width);
    azimuth = azimuth * DEG_TO_RAD;
    tracks_msg_.tracks[track_id].id = track_id;
    tracks_msg_.tracks[track_id].pos.x = cos(azimuth) * range;
    tracks_msg_.tracks[track_id].pos.y = sin(azimuth) * range;
    tracks_msg_.tracks[track_id].vel.x = range_rate;
    tracks_msg_.tracks[track_id].vel.y = lat_rate;
    tracks_msg_.tracks[track_id].acc.x = cos(azimuth) * range_accel;
    tracks_msg_.tracks[track_id].acc.y = sin(azimuth) * range_accel;

    tracks_msg_.tracks[track_id].status = status;
    tracks_msg_.tracks[track_id].width = width;
    if (width > 0.0)
    {
      std::cout << width << std::endl;
    }
  }

  void PeCanEsrParser::parseTrackExtras(const can::Frame &f)
  {
    int track_id, track_group;
    float amplitude;
    bool moving, movable_slow, movable_fast;

    ESR_TRACK_EXTRAS_GROUP.parseValue(f, &track_group);
    if (track_group < (ESR_MAX_TRACK_EXTRAS_NUMBER - 1))
    {
      ESR_TRACK_EXTRAS_AMPLITUDE_1.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_1.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_1.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_1.parseValue(f, &movable_fast);
      track_id = 0 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;

      ESR_TRACK_EXTRAS_AMPLITUDE_2.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_2.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_2.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_2.parseValue(f, &movable_fast);
      track_id = 1 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;

      ESR_TRACK_EXTRAS_AMPLITUDE_3.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_3.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_3.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_3.parseValue(f, &movable_fast);
      track_id = 2 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;

      ESR_TRACK_EXTRAS_AMPLITUDE_4.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_4.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_4.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_4.parseValue(f, &movable_fast);
      track_id = 3 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;

      ESR_TRACK_EXTRAS_AMPLITUDE_5.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_5.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_5.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_5.parseValue(f, &movable_fast);
      track_id = 4 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;

      ESR_TRACK_EXTRAS_AMPLITUDE_6.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_6.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_6.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_6.parseValue(f, &movable_fast);
      track_id = 5 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;

      ESR_TRACK_EXTRAS_AMPLITUDE_7.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_7.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_7.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_7.parseValue(f, &movable_fast);
      track_id = 6 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;

    } else
    {
      ESR_TRACK_EXTRAS_AMPLITUDE_1.parseValue(f, &amplitude);
      ESR_TRACK_EXTRAS_MOVING_1.parseValue(f, &moving);
      ESR_TRACK_EXTRAS_MOVABLE_SLOW_1.parseValue(f, &movable_slow);
      ESR_TRACK_EXTRAS_MOVABLE_FAST_1.parseValue(f, &movable_fast);
      track_id = 0 + track_group * 7;
      tracks_msg_.tracks[track_id].amplitude = amplitude;
      tracks_msg_.tracks[track_id].is_movable =
          moving * 100 + movable_slow * 10 + movable_fast;
    }
  }

  void PeCanEsrParser::aggregateTracks(const can::Frame &f)
  {
    int track_id;
    bool to_publish = false;
    track_id = f.id - ESR_TRACK_START;

    if (track_id == 0)
    {
      tracks_msg_.header.stamp = ros::Time::now();
      first_track_arrived_ = true;
      track_count_ = 1;
      track_extras_count_ = 0;
    } else if (track_id < ESR_MAX_TRACK_NUMBER)
    {
      track_count_ += 1;
    }

    if (f.id == ESR_TRACK_EXTRAS)
    {
      track_extras_count_++;
    }

    if (track_count_ == ESR_MAX_TRACK_NUMBER &&
        track_extras_count_ == ESR_MAX_TRACK_EXTRAS_NUMBER)
    {
      to_publish = true;
      track_array_topic_.publish(tracks_msg_);
    }
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pe_can_esr_parser");
  radar_interface::PeCanEsrParser node;
  node.spin();
  return 0;
}
